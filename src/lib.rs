//! Macros for unwrapping known values from `enum`s with single contained values.

#[macro_export]
macro_rules! variant_or_none {
    ($enum:expr, $variant:path $(,)?) => {
        match ($enum) {
            $variant(v) => Some(v),
            _ => None,
        }
    };
}

#[macro_export]
macro_rules! expect_variant {
    ($enum:expr, $variant:path, $expectation:expr $(,)?) => {
        $crate::variant_or_none!($enum, $variant).expect($expectation)
    };
}

#[macro_export]
macro_rules! unwrap_variant {
    ($enum:expr, $variant:path) => {
        $crate::expect_variant!(
            $enum,
            $variant,
            &format!("expected {}", stringify!($variant)),
        )
    };
}

#[cfg(test)]
mod test {
    #[allow(dead_code)]
    enum Foo {
        Var1(u32),
        Var2(String),
        Var3(u64, u64),
    }

    #[test]
    fn variant_or_none_matching() {
        assert_eq!(variant_or_none!(Foo::Var1(42), Foo::Var1), Some(42));
    }

    #[test]
    fn variant_or_none_nonmatching() {
        assert_eq!(variant_or_none!(Foo::Var3(1, 2), Foo::Var2), None);
    }

    #[test]
    fn unwrap_variant_matching() {
        assert_eq!(unwrap_variant!(Foo::Var1(42), Foo::Var1), 42);
    }

    #[test]
    #[should_panic]
    fn unwrap_variant_nonmatching() {
        unwrap_variant!(Foo::Var3(1, 2), Foo::Var2);
    }
}
