Macros for `enum` unwrapping
============================

This crate provides useful macros for unwrapping values contained in Rust
`enum`s where each variant has a single field.

The `enum`s that can be used look like this:

```rust
enum Foo {
	Variant1(String),
	Variant2(u32),
	Variant3(&'static str),
}
```

The following macros are provided:

  * `variant_or_none!(expr, variant)` which extracts value from the `enum`
    if the specified variant is provided, returns `None` when the expression
	is not the specified variant.

  * `expect_variant!(expr, variant, message)` which also extracts the value,
    but instead of wrapping the value in `Option`, panics with the provided
	message when the expression does not have the specified variant.

  * `unwrap_variant!(expr, variant)` which does the same as the previous one,
    but the message is provided automatically.